#include "core/game.hpp"
#include "moon/moon.hpp"

#include <SFML/Graphics.hpp>

#include <vector>
#include <iostream>
#include "asteroids/asteroid.hpp"
#include "asteroids/asteroidHandler.hpp"

#include "moon/projectileHandler.hpp"

#include "core/collisionHandler.hpp"
#include "ui/upgradeMenu.hpp"
#include "ui/start.hpp"


int main()
{
    core::game::startup();

    sf::RectangleShape bg(WINDOW_SIZE);
    bg.setOrigin(WINDOW_SIZE.x/2, WINDOW_SIZE.y/2);
    bg.setPosition(0, 0);
    bg.setTexture(core::textureManager::getTexture("background"));
    core::game::addElementToDraw(&bg);
   
    Moon & lune = Moon::get();
    AsteroidHandler::get();
    UpgradeMenu & upgradeMenu = UpgradeMenu::get();

    core::game::addElementToDraw(&lune);
    core::game::addActor(&lune);

    core::game::addActor(&upgradeMenu, core::game::ScreenType::Shop);
    core::game::addElementToDraw(&upgradeMenu, core::game::ScreenType::Shop);
    
    StartMenu & startMenu = StartMenu::get();
    core::game::addActor(&startMenu, core::game::ScreenType::Start);
    core::game::addElementToDraw(&startMenu, core::game::ScreenType::Start);

    CollisionHandler colHandler;

    core::game::addActor(&colHandler);

    core::game::run();

    return 0;
}