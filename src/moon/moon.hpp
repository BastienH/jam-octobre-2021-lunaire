#pragma once

#include <SFML/Graphics.hpp>
#include "core/game.hpp"
#include "core/textureManager.hpp"

#include "moon/projectileHandler.hpp"

#include "core/actor.hpp"

#include <cmath>
#include <memory>

constexpr float MOON_RADIUS{100};
constexpr std::chrono::milliseconds MAX_COOLDOWN{std::chrono::milliseconds(700)};
constexpr std::chrono::milliseconds MAX_ROTATION_DURATION{std::chrono::seconds(3)};

class Moon : public sf::Drawable, public Actor
{
    private:
        sf::CircleShape _fg;
        sf::CircleShape _bg;
        std::chrono::milliseconds _rotationDuration;
        std::chrono::milliseconds _cd, _cdMax;
        double _rotationSpeed;
        unsigned _numberOfShoot{1};

    // Singleton
        static std::unique_ptr<Moon> _instance;
    // ----
    public:
        Moon();
        static Moon & get();
        void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;        
        void update(std::chrono::milliseconds dt) override;
        void multiplyRotationSpeed(double factor) {_rotationSpeed *= factor;}
        void multiplyShootingRate(double factor) {_cdMax *= factor;}
        void moarPew() {_numberOfShoot++;};
        const sf::CircleShape& getShape() const { return _fg; }
};