#include "moon.hpp"

std::unique_ptr<Moon> Moon::_instance = nullptr;

Moon::Moon():     
    _fg(MOON_RADIUS),
    _bg(MOON_RADIUS),
    _rotationDuration(MAX_ROTATION_DURATION),
    _cd(0), _cdMax(MAX_COOLDOWN),
    _rotationSpeed(0)
{   
    _bg.setOrigin(MOON_RADIUS, MOON_RADIUS);
    _bg.setPosition(0, 0);
    _bg.setTexture(core::textureManager::getTexture("moon_bg"));

    _fg.setOrigin(MOON_RADIUS, MOON_RADIUS);
    _fg.setPosition(0, 0);
    _fg.setTexture(core::textureManager::getTexture("moon_fg"));

}

void Moon::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
{
    rt.draw(_fg, rs);
    rt.draw(_bg, rs);
}

void Moon::update(std::chrono::milliseconds dt)
{
    _rotationSpeed = 360.0/_rotationDuration.count();

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
        _bg.rotate(-_rotationSpeed*dt.count());
    }
    
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        _bg.rotate(_rotationSpeed*dt.count());
    }
    
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
    {
        if(_cd <= std::chrono::milliseconds(0))
        {
            _cd = _cdMax;
            const double angleBase = _bg.getRotation() - (_numberOfShoot * 3 / 2); // Degré
            for(auto i = 0u; i < _numberOfShoot; ++i)
            {
                const double angleOffset = i * 3; // 3 Degrés entre chaque tire
                const auto angle = (angleBase + angleOffset) * 2 * M_PI / 360.f - M_PI_2; // Rad
                ProjectileHandler::get().create(sf::Vector2f(cos(angle) * MOON_RADIUS, sin(angle) * MOON_RADIUS), angle);
            }
        }
    }

    _cd -= dt;
}

Moon & Moon::get()
{
    if (!_instance)
    {
        _instance = std::make_unique<Moon>();
    }
    return *_instance;
}