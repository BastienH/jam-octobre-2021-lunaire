#pragma once

#include "core/actor.hpp"
#include "core/textureManager.hpp"

#include "common.hpp"

#include <SFML/Graphics.hpp>
#include <cmath>

class Projectile : public sf::Drawable, public Actor
{
    private:
        bool _isDead;
        sf::CircleShape _shape;
    
    public:
        static double _maxSpeed;
        static double _size;
        static unsigned _damages;
    private:
        sf::Vector2f _speed{0, 0};

    public:
        Projectile(const sf::Vector2f& start, double angle):
        _isDead(false)
        {
            _shape.setRadius(_size);
            _shape.setOrigin(_size, _size);
            _shape.setPosition(start);

            _shape.setTexture(core::textureManager::getTexture("proj"));

            _speed = sf::Vector2f(cos(angle)*_maxSpeed, sin(angle)*_maxSpeed);
        }

        void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override
        {
            rt.draw(_shape, rs);
        }

        void update(std::chrono::milliseconds dt) override
        {
            if(!isDead())
            {
                _shape.move({_speed.x*dt.count(), _speed.y*dt.count()});
                const auto& pos = _shape.getPosition();
                if(abs(pos.x) > WINDOW_SIZE.x)
                {
                    destroy();
                }
            }
        }

        void destroy() { _isDead = true; }

        bool isDead() const { return _isDead; }

        const sf::CircleShape& getShape() const { return _shape; }

        unsigned getDamages() const { return _damages; }
};