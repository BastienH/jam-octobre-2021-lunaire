#pragma once

#include "moon/projectile.hpp"
#include <memory>
#include <vector>
#include <algorithm>

#include "core/game.hpp"    
class ProjectileHandler : public sf::Drawable, public Actor
{
    private:
        static std::unique_ptr<ProjectileHandler> _handler;

        std::vector<std::unique_ptr<Projectile>> _projectiles;
        
    public:
        ProjectileHandler(){}

        static ProjectileHandler& get() 
        { 
            if(!_handler)
            {
                _handler = std::make_unique<ProjectileHandler>();
                core::game::addElementToDraw(_handler.get());
                core::game::addActor(_handler.get());
            }

            return *_handler; 
        }

        void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override
        {   
            std::for_each(_projectiles.begin(), _projectiles.end(), [&rt, &rs](const auto& proj)
            {
                rt.draw(*proj, rs);
            });
        }

        void update(std::chrono::milliseconds dt) override
        {
            std::for_each(_projectiles.begin(), _projectiles.end(), [dt](auto& proj)
            {
                proj->update(dt);
            });

            clean();
        }

        Projectile& create(const sf::Vector2f& start, double angle)
        {
            _projectiles.emplace_back(std::make_unique<Projectile>(start, angle));

            return *_projectiles.back();
        }

        void clean()
        {
            _projectiles.erase(std::remove_if(_projectiles.begin(), _projectiles.end(), [](const auto& elem)
            {
                return elem->isDead();        
            }
            ), _projectiles.end());
        }

        std::vector<std::unique_ptr<Projectile>>& getList() { return _projectiles; }
};