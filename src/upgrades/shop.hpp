#pragma once

#include <array>

namespace shop
{
    constexpr double rotation_speed_factor = 0.8;
    constexpr double projectile_speed_factor = 1.5;

    enum class UpgradeType
    {
        MovementSpeed = 0,
        ProjectileSpeed,
        ShootRate,
        Damage,
        Shooter,
        Moulaga,
    };

    struct upgrade
    {
        unsigned cost;
        unsigned max_level; //Exclu
        unsigned level = 0;
    };

    extern unsigned _gold;
    extern unsigned _score;
    extern std::array<upgrade, 6> _upgrades;

    
    void addGold(unsigned amount);
    void addScore(unsigned amount);

    void buy(UpgradeType);
}