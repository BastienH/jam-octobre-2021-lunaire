#include "upgrades/shop.hpp"
#include <iostream>

#include "moon/moon.hpp"
#include "moon/projectile.hpp"

namespace shop
{
    unsigned _gold{0};
    unsigned _score{0};
    double _multiplier{1};

    std::array<upgrade, 6> _upgrades = {
        upgrade{100, 5},
        upgrade{120, 5},
        upgrade{130, 5},
        upgrade{160, 5},
        upgrade{200, 5},
        upgrade{400, 5}
    };

    void addGold(unsigned amount)
    {
        _gold += amount * _multiplier;
    }    
    
    void addScore(unsigned amount)
    {
        _score += amount;
    }

    void buy(UpgradeType up)
    {
        if (_upgrades[static_cast<int>(up)].level < _upgrades[static_cast<int>(up)].max_level && _upgrades[static_cast<int>(up)].cost <= _gold)
        {
            _upgrades[static_cast<int>(up)].cost *= 2;
            _upgrades[static_cast<int>(up)].level++;

            switch (up)
            {
            case UpgradeType::MovementSpeed:
                Moon::get().multiplyRotationSpeed(rotation_speed_factor);
                break;
            case UpgradeType::ProjectileSpeed:
                Projectile::_maxSpeed *= projectile_speed_factor;
                break;
            case UpgradeType::ShootRate:
                Moon::get().multiplyShootingRate(1.1);
                break;
            case UpgradeType::Damage:
                Projectile::_damages++;
                break;
            case UpgradeType::Shooter:
                Moon::get().moarPew();
                break;
            case UpgradeType::Moulaga:
                _multiplier *= 1.5;
            default:
                break;
            }
        }
    }
}