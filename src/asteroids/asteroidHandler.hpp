#pragma once

#include "core/actor.hpp"
#include "asteroids/asteroid.hpp"
#include <SFML/Graphics.hpp>
#include <memory>
#include "asteroids/popup.hpp"

struct AsteroidHandler : Actor, sf::Drawable
{
    void update(std::chrono::milliseconds) override;
    void draw(sf::RenderTarget &, sf::RenderStates) const override;
    ~AsteroidHandler();
    static AsteroidHandler & get();
    void pushNewAsteroid();
    void removeAll();
    std::vector<std::unique_ptr<Asteroid>>& getList() { return _asteroids; }

private:
    static std::unique_ptr<AsteroidHandler> _instance;
    std::vector<std::unique_ptr<Asteroid>> _asteroids;
    std::vector<Popup> _popups;
};