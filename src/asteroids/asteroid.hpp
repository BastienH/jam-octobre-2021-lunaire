#pragma once

#include <SFML/Graphics.hpp>
#include <ostream>
#include <random>
#include "core/actor.hpp"

enum class AsteroidSize {Tiny, Medium, Big};

constexpr unsigned SIGN_OFFSET{15};
constexpr unsigned SIGN_SIZE{10};

enum class Status { Alive, Dead_by_proj, Dead_by_moon, OOB };

struct Asteroid: sf::Drawable, Actor
{
private:
    sf::CircleShape _shape;
    sf::CircleShape _sign;
    Asteroid(int size, int x, int y);
    static std::mt19937 gen;
    static std::normal_distribution<> sizeDistrib;
    static std::uniform_real_distribution<> positionDistrib;
    static std::normal_distribution<> rotationDistrib;

    std::chrono::milliseconds travelTime{std::chrono::seconds(10)};

    float _speedX; 
    float _speedY;

    Status _shouldBeDestroyed = Status::Alive;
    
    bool _showSign{true};

    int _hp;
    unsigned _value;

    float _rotationSpeed;

    std::string _textureId{};
    unsigned _state{0};
public:
    static Asteroid generate();
    void draw(sf::RenderTarget &, sf::RenderStates) const override;
    void update(std::chrono::milliseconds) override;
    Status shouldBeDestroyed() const;
    friend std::ostream & operator<<(std::ostream &, Asteroid const &);
    void destroy(unsigned dmg, Status from);
    const sf::CircleShape& getShape() const { return _shape; }
    unsigned getValue() const { return _value; }
    AsteroidSize getType() const;
};

std::ostream & operator<<(std::ostream &, Asteroid const &);