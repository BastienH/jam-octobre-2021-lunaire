#include "asteroid.hpp"

#include "common.hpp"
#include "core/game.hpp"
#include "core/textureManager.hpp"
#include <cmath>

#include "upgrades/shop.hpp"

std::mt19937 Asteroid::gen = std::mt19937{426903051998};
std::normal_distribution<> Asteroid::sizeDistrib = std::normal_distribution<>{50, 20};
std::uniform_real_distribution<> Asteroid::positionDistrib = std::uniform_real_distribution<>{0, 2 * M_PI};
std::normal_distribution<> Asteroid::rotationDistrib = std::normal_distribution<>{0, 0.25};

Asteroid::Asteroid(int size, int x, int y):
    _shape(size),
    _sign(SIGN_SIZE)
{
    constexpr const char* textures[] = {"tiny", "medium", "big"};
    constexpr unsigned hp[] = {1, 2, 4};
    constexpr unsigned value[] = {10, 20, 40};
    constexpr std::chrono::seconds travelTimes[] = {std::chrono::seconds(8),
                                                    std::chrono::seconds(10),
                                                    std::chrono::seconds(12)};

    const auto& type = static_cast<int>(getType());

    _textureId = textures[type];

    travelTime = travelTimes[type];

    _shape.setTexture(core::textureManager::getTexture(_textureId + "_0"));
    _shape.setOrigin(size, size);
    _shape.setPosition(x, y);

    _sign.setOrigin(SIGN_SIZE, SIGN_SIZE);
    const auto diag = std::hypot(WINDOW_SIZE.x, WINDOW_SIZE.y) / 2;
    const auto signRadius = WINDOW_SIZE.y/2 - SIGN_OFFSET;
    _sign.setPosition(x/diag * signRadius, y/diag * signRadius);
    _sign.setTexture(core::textureManager::getTexture("sign"));

    _hp = hp[type];
    _value = value[type];

    _speedX = -_shape.getPosition().x / travelTime.count();
    _speedY = -_shape.getPosition().y / travelTime.count();

    _rotationSpeed = 0.25 - rotationDistrib(gen);
}

Asteroid Asteroid::generate()
{
    const auto diag = std::hypot(WINDOW_SIZE.x, WINDOW_SIZE.y) / 2;
    double angle = positionDistrib(gen);
    return Asteroid(sizeDistrib(gen), diag * cos(angle), -1 * diag * sin(angle));
}

void Asteroid::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
   if(_showSign)
   {
       target.draw(_sign, states);
   }
   
   target.draw(_shape, states);
}

void Asteroid::update(std::chrono::milliseconds dt)
{
    _shape.rotate(_rotationSpeed);
    _shape.move(sf::Vector2f{_speedX * dt.count(), _speedY * dt.count()});
    if (abs(_shape.getPosition().x) > WINDOW_SIZE.x * 2)
        _shouldBeDestroyed = Status::OOB;

    const auto& pos = _shape.getPosition();
    if(_showSign && abs(pos.x) < WINDOW_SIZE.x/2 && abs(pos.y) < WINDOW_SIZE.y/2)
    {
        _showSign = false;
    }
}

Status Asteroid::shouldBeDestroyed() const
{
    return _shouldBeDestroyed;
}

void Asteroid::destroy(unsigned dmg, Status from) 
{ 
    _hp -= dmg;
    _state += 1;

    if(_hp <= 0)
    {
        _shouldBeDestroyed = from; 

        if(from == Status::Dead_by_proj)
        {
            shop::addGold(_value);
            auto score = (4-static_cast<unsigned>(getType()))*10;
            shop::addScore(score);
        }
    }
    else
    {
        _shape.setTexture(core::textureManager::getTexture(_textureId + "_" + std::to_string(_state)));
    }
}

AsteroidSize Asteroid::getType() const
{
    const auto& radius = _shape.getRadius();
    const auto min = sizeDistrib.mean() - sizeDistrib.stddev();
    const auto max = sizeDistrib.mean() + sizeDistrib.stddev();

    if(radius < min)
    {
        return AsteroidSize::Tiny;
    }
    else if(radius > max)
    {
        return AsteroidSize::Big;
    }
    else
    {
        return AsteroidSize::Medium;
    }
}

std::ostream & operator<<(std::ostream & s, Asteroid const & a)
{
    s << '(' << a._shape.getPosition().x << "; " << a._shape.getPosition().y << "; " << a._shape.getRadius() << ')';
    return s;
}

