#pragma once

#include "core/actor.hpp"
#include "core/game.hpp"
#include "core/textureManager.hpp"

#include <SFML/Graphics.hpp>

constexpr unsigned ImgInSpritesheet{9};
const sf::Vector2f ImgSize{600,567};

class Popup: public Actor, public sf::Drawable
{
    private:
        sf::Text _text;
        bool _isDead{false};

        //Explosion
        sf::RectangleShape _explosion;
        unsigned _frame{0};
        std::chrono::milliseconds _timer{0};
        std::chrono::milliseconds _delay{42};
        sf::IntRect _textureRect;
    public:
        Popup(unsigned value, const sf::CircleShape& shape):
        _explosion(sf::Vector2f(shape.getRadius()*2, shape.getRadius()*2))
        {
            _text.setFont(core::game::getFont());
            _text.setString(std::to_string(value) + "¤");
            _text.setCharacterSize(20);
            _text.setOrigin(_text.getGlobalBounds().width/2, _text.getGlobalBounds().height/2);
            _text.setPosition(shape.getPosition());

            _explosion.setOrigin(shape.getOrigin());
            _explosion.setPosition(shape.getPosition());
            _explosion.setTexture(core::textureManager::getTexture("explosion"));
            _explosion.setTextureRect(sf::IntRect(sf::Vector2<int>(0, 0), sf::Vector2<int>(ImgSize)));
        }

        void update(std::chrono::milliseconds dt) override
        {
            _timer += dt;

            if(_timer >= _delay)
            {
                _timer = std::chrono::milliseconds(0);

                ++_frame;

                if(_frame < ImgInSpritesheet)
                {
                    _explosion.setTextureRect(sf::IntRect(sf::Vector2<int>(_frame*ImgSize.x, 0), sf::Vector2<int>(ImgSize)));
                }
                else
                {
                    _isDead = true;
                }
            }
        }

        void draw(sf::RenderTarget & rt, sf::RenderStates rs) const override
        {
            rt.draw(_explosion, rs);
            rt.draw(_text, rs);
        }

        bool isDead() const { return _isDead; }

};   