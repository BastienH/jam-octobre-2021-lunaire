#include "asteroidHandler.hpp"
#include "core/game.hpp"

#include <functional>

std::unique_ptr<AsteroidHandler> AsteroidHandler::_instance = nullptr;

AsteroidHandler & AsteroidHandler::get()
{
    if (!_instance)
    {
        _instance = std::make_unique<AsteroidHandler>();
        core::game::addActor(_instance.get());
        core::game::addElementToDraw(_instance.get());
    }
    return *_instance;
}

void AsteroidHandler::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    for (auto & a : _asteroids)
        a->draw(target, states);

    for (auto & p : _popups)
    {
        p.draw(target, states);
    }
}

void AsteroidHandler::update(std::chrono::milliseconds dt)
{
    for (auto & a : _asteroids)
    {
        a->update(dt);

        if(a->shouldBeDestroyed() == Status::Dead_by_proj)
        {
            _popups.emplace_back(a->getValue(), a->getShape());
        }
    }

    for (auto & p : _popups)
    {
        p.update(dt);
    }

    _asteroids.erase(std::remove_if(_asteroids.begin(), _asteroids.end(), [](auto & a){return a->shouldBeDestroyed() != Status::Alive;}), _asteroids.end());

    _popups.erase(std::remove_if(_popups.begin(), _popups.end(), [](auto & a){return a.isDead();}), _popups.end());
}

AsteroidHandler::~AsteroidHandler()
{
    removeAll();
}

void AsteroidHandler::pushNewAsteroid()
{
    _asteroids.push_back(std::make_unique<Asteroid>(Asteroid::generate()));
}

void AsteroidHandler::removeAll()
{
    _asteroids.clear();
}