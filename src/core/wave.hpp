#pragma once

#include <vector>
#include <chrono>
#include <random>

#include <sstream>
#include "core/actor.hpp"

struct Wave : Actor
{
    std::vector<std::chrono::milliseconds> _timestamps; // Tout les timings auquels faire spawn un asteroid
    std::chrono::seconds waveLength = std::chrono::seconds(60);

    std::chrono::time_point<std::chrono::system_clock> _startTime;
    
    Wave();
    ~Wave();
    void update(std::chrono::milliseconds);
    bool isFinished() const;
    std::string getTanKiRest() const;
private:
    static std::mt19937 gen;
    static std::normal_distribution<> timeDistrib;

    static unsigned numberOfAsteroid;
    
};