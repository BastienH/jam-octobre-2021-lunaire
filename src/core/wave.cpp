#include "wave.hpp"
#include "asteroids/asteroidHandler.hpp"
#include "core/game.hpp"
#include <iostream>

std::mt19937 Wave::gen = std::mt19937{426903051998};
std::normal_distribution<> Wave::timeDistrib = std::normal_distribution<>{0, 0.5};
unsigned Wave::numberOfAsteroid{18};


Wave::Wave()
{
    const auto delay = std::chrono::seconds(60/numberOfAsteroid);

    numberOfAsteroid += 2;

    for (auto i = 0u; i < numberOfAsteroid; ++i)
    {
        auto t = timeDistrib(gen); // 7 est supposé comme étant le max de la distribution.
        _timestamps.push_back(std::chrono::duration_cast<std::chrono::milliseconds>(delay*(i+t)));
    }

    std::sort(_timestamps.begin(), _timestamps.end());

    _startTime = std::chrono::system_clock::now();

    core::game::addActor(this);
}

Wave::~Wave()
{
    core::game::removeActor(this);
}

void Wave::update(std::chrono::milliseconds)
{
    const auto waveDuration = std::chrono::system_clock::now() - _startTime;
    for (auto i = 0u; i < _timestamps.size() && waveDuration > _timestamps[i]; ++i)
    {
        AsteroidHandler::get().pushNewAsteroid();
    }

    _timestamps.erase(std::remove_if(_timestamps.begin(), _timestamps.end(), [&waveDuration](auto & t){return t < waveDuration;}), _timestamps.end());
}

bool Wave::isFinished() const
{
    const auto now = std::chrono::system_clock::now();
    return now - _startTime > 1.1 * waveLength;
}

std::string Wave::getTanKiRest() const
{
    std::ostringstream oss;
    const auto d = waveLength - std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - _startTime);
    oss << std::chrono::duration_cast<std::chrono::seconds>(d).count() << "s"; 

    return oss.str();
}