#pragma once


#include <map>
#include <memory>
#include <string>

namespace sf
{
class Texture;
} // namespace sf

namespace core::textureManager
{
    extern std::map<std::string, std::unique_ptr<sf::Texture>> _textures;
    
    void load(const std::string& path,const std::string& id);

    sf::Texture* getTexture(const std::string& id);
}