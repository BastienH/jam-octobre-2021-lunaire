#include "core/game.hpp"
#include "core/textureManager.hpp"

#include "common.hpp"

#include <SFML/Graphics/RenderWindow.hpp> // for RenderWindow
#include <SFML/Window/Event.hpp>		  // for Event, Event::Closed
#include <SFML/Window/VideoMode.hpp>	  // for VideoMode
#include <SFML/Graphics.hpp>

#include <chrono>
#include <algorithm>
#include <array>
#include <iostream>

#include "ui/upgradeMenu.hpp"
#include "ui/start.hpp"

#include "core/wave.hpp"
#include <string>
#include "asteroids/asteroidHandler.hpp"
#include "upgrades/shop.hpp"

#include "upgrades/shop.hpp"

namespace core::game
{
    ScreenType _actualScreenType = ScreenType::Start;
    std::unique_ptr<sf::RenderWindow> _window = nullptr;
    std::unique_ptr<sf::Font> _font = nullptr;
    bool _isRunning = false;
    bool _isStarted = false;
    std::array<std::vector<sf::Drawable*>, 4> _node{};
    std::array<std::vector<Actor*>,4> _actors{};

    sf::Text textTimer, textGold, textScore;
    
   void startup()
    {
         
        _isRunning		= false;

        _window = std::make_unique<sf::RenderWindow>(sf::VideoMode(WINDOW_SIZE.x, WINDOW_SIZE.y), "Space Billy");

        _window->setFramerateLimit(120);

        _font = std::make_unique<sf::Font>();
        _font->loadFromFile("resources/font/font.ttf");

        core::textureManager::load("resources/wallpaper.png", "background");
        core::textureManager::load("resources/bg.png", "moon_bg");    
        core::textureManager::load("resources/fg.png", "moon_fg");   
        core::textureManager::load("resources/sign.png", "sign");
        
        //Asteroids
        core::textureManager::load("resources/asteroids/big/big_0.png", "big_0");
        core::textureManager::load("resources/asteroids/big/big_1.png", "big_1");
        core::textureManager::load("resources/asteroids/big/big_2.png", "big_2");
        core::textureManager::load("resources/asteroids/big/big_3.png", "big_3");

        core::textureManager::load("resources/asteroids/medium/medium_0.png", "medium_0");
        core::textureManager::load("resources/asteroids/medium/medium_1.png", "medium_1");

        core::textureManager::load("resources/asteroids/tiny/tiny_0.png", "tiny_0");

        //Projectile
        core::textureManager::load("resources/proj.png", "proj");

        //Explosion
        core::textureManager::load("resources/explosion.png", "explosion");
        core::textureManager::load("resources/big.png", "big");
        core::textureManager::load("resources/medium.png", "medium");
        core::textureManager::load("resources/tiny.png", "tiny");        

        //Menu
        core::textureManager::load("resources/icons/damage.png", "ico_damage");
        core::textureManager::load("resources/icons/money.png", "ico_money");
        core::textureManager::load("resources/icons/speed.png", "ico_speed");
        core::textureManager::load("resources/icons/muti_bullet.png", "ico_multibullet");
        core::textureManager::load("resources/icons/proj_speed.png", "ico_proj_speed");
        core::textureManager::load("resources/icons/rate.png", "ico_rate");
        core::textureManager::load("resources/icons/bg.png", "upgrade_bg");


        sf::View view(sf::Vector2f(0, 0), WINDOW_SIZE);

        _window->setView(view);

        // Element de l'UI
        textTimer.setFont(getFont());
        textGold.setFont(getFont());
        textScore.setFont(getFont());

        textTimer.setPosition(-textTimer.getGlobalBounds().width/2, -WINDOW_SIZE.y/2);
        textGold.setPosition(-WINDOW_SIZE.x / 2, -WINDOW_SIZE.y/2);
        textScore.setPosition(+WINDOW_SIZE.x / 2 - textScore.getGlobalBounds().width, -WINDOW_SIZE.y/2);

        textGold.setString("Argent : ");
        textScore.setString("Score : ");

        textTimer.setCharacterSize(20);
        textGold.setCharacterSize(20);
        textScore.setCharacterSize(20);

        //addElementToDraw(&textTimer);
        //addElementToDraw(&textGold);
        //addElementToDraw(&textScore);
    }

    void run()
    {
        Wave vvave;
        if (_isRunning)
        {
            return;
        }
        _isRunning = true;

        std::chrono::milliseconds deltaTime{0};

        while (_isRunning && _window->isOpen())
        {
            auto start = std::chrono::high_resolution_clock::now();

            sf::Event event;
            while (_window->pollEvent(event))
            {
                if(_actualScreenType == ScreenType::Shop)
                {
                    UpgradeMenu::get().eventUpdate(event);
                }
                else if(_actualScreenType == ScreenType::Start)
                {
                    StartMenu::get().eventUpdate(event);
                }
                
                if (event.type == sf::Event::Closed)
                {
                    stop();
                    return;
                }
            }

            _window->clear();

            std::for_each(_node[static_cast<int>(_actualScreenType)].begin(), _node[static_cast<int>(_actualScreenType)].end(), [](const auto& elem)
            {
                _window->draw(*elem);
            });

            if(_actualScreenType == ScreenType::InGame)
            {
                _window->draw(textTimer);
                _window->draw(textGold);
                _window->draw(textScore);
            }

            _window->display();

            auto end  = std::chrono::high_resolution_clock::now();
            deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

            std::for_each(_actors[static_cast<int>(_actualScreenType)].begin(), _actors[static_cast<int>(_actualScreenType)].end(), [deltaTime](const auto& a)
            {
                a->update(deltaTime);
            });

            textTimer.setString(vvave.getTanKiRest());
            textTimer.setPosition(-textTimer.getGlobalBounds().width/2, -WINDOW_SIZE.y/2);

            textGold.setString("Argent : " + std::to_string(shop::_gold));

            textScore.setString(std::string("Score : ") + std::to_string(shop::_score));
            textScore.setPosition(+WINDOW_SIZE.x / 2 - textScore.getGlobalBounds().width, -WINDOW_SIZE.y/2);

            if(_isStarted)
            {
                _isStarted = false;
                _actualScreenType = ScreenType::InGame;
                vvave = Wave();
            }

            if (_actualScreenType == ScreenType::InGame)
            {
                if (vvave.isFinished())
                {
                    shop::addScore(100);
                    _actualScreenType = ScreenType::Shop;
                    AsteroidHandler::get().removeAll();
                }
            }
            else if (_actualScreenType == ScreenType::Shop)
            {
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                {
                    _actualScreenType = ScreenType::InGame;
                    vvave = Wave();
                }
            }
        }
    }

    void stop()
    {
        std::cout << "Score : " << std::to_string(shop::_score) << std::endl;
        _isRunning = false;
        _window->close();
    }

    void addElementToDraw(sf::Drawable* element, ScreenType screen)
    {
        _node[static_cast<int>(screen)].push_back(element);
    }

    void removeElementToDraw(sf::Drawable* element, ScreenType screen)
    {
        _node[static_cast<int>(screen)].erase(std::remove(_node[static_cast<int>(screen)].begin(), _node[static_cast<int>(screen)].end(), element), _node[static_cast<int>(screen)].end());
    }

    void addActor(Actor * actor, ScreenType screen)
    {
        _actors[static_cast<int>(screen)].push_back(actor);
    }

    void removeActor(Actor * actor, ScreenType screen)
    {
        _actors[static_cast<int>(screen)].erase(std::remove(_actors[static_cast<int>(screen)].begin(), _actors[static_cast<int>(screen)].end(), actor), _actors[static_cast<int>(screen)].end());
    }

    sf::RenderWindow& getWindow() { return *_window; }

    sf::Font& getFont() { return *_font; }
}