#pragma once

#include "moon/projectileHandler.hpp"
#include "moon/moon.hpp"

#include "asteroids/asteroidHandler.hpp"

#include <algorithm>

#include <cmath>
#include <iostream>

constexpr double OFFSET_COLLISION{15};

struct CollisionHandler: public Actor
{
    void computeCollisions()
    {
        auto& projList = ProjectileHandler::get().getList();
        auto& asteroidList = AsteroidHandler::get().getList();

        std::for_each(asteroidList.begin(), asteroidList.end(), [&projList, this](auto& asteroid)
        {
            const auto& asteroidRadius = asteroid->getShape().getRadius();
            const auto& asteroidPos = asteroid->getShape().getPosition();

            for(auto& proj : projList)
            {
                const auto& projPos = proj->getShape().getPosition();
                const auto& projRadius = proj->getShape().getRadius();
                
                if(std::hypot(asteroidPos.x - projPos.x, asteroidPos.y - projPos.y) < asteroidRadius + projRadius)
                {
                    proj->destroy();
                    asteroid->destroy(proj->getDamages(), Status::Dead_by_proj);
                    break;
                }
            }
            
            if(std::hypot(asteroidPos.x, asteroidPos.y) < asteroidRadius + MOON_RADIUS - OFFSET_COLLISION)
            {
                std::cout << "An asteroid destroyed the moon !" << std::endl; 
                asteroid->destroy(1000, Status::Dead_by_moon);
                core::game::stop();
            }
        });
    }

    void update(std::chrono::milliseconds) override
    {
        computeCollisions();
    }
};