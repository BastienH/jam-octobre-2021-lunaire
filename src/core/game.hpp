#pragma once

#include <memory>	  // for unique_ptr, swap, make_unique
#include <vector>

#include "actor.hpp"

namespace sf
{
class RenderWindow;
class Drawable;
class Font;
} // namespace sf

namespace core::game
{
    enum class ScreenType
    {
        InGame, Shop, Start, Finish
    };

    extern ScreenType _actualScreenType;
    extern std::unique_ptr<sf::RenderWindow> _window;
    
    extern bool _isRunning;
    extern bool _isStarted;

    void startup();
    void run();
    void stop();

    void addElementToDraw(sf::Drawable* element, ScreenType screen = ScreenType::InGame);
    void removeElementToDraw(sf::Drawable* element, ScreenType screen = ScreenType::InGame);
    void addActor(Actor* element, ScreenType screen = ScreenType::InGame);
    void removeActor(Actor* element, ScreenType screen = ScreenType::InGame);

    sf::RenderWindow& getWindow();

    sf::Font& getFont();

}