#pragma once

#include <chrono>

struct Actor
{
    virtual void update(std::chrono::milliseconds dt) = 0;
};