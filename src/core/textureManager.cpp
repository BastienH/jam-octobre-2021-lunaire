#include "core/textureManager.hpp"

#include <SFML/Graphics.hpp>

#include <stdexcept>

namespace core::textureManager
{
    std::map<std::string, std::unique_ptr<sf::Texture>> _textures{};
    
    void load(const std::string& path,const std::string& id)
    {
        std::unique_ptr<sf::Texture> texture = std::make_unique<sf::Texture>();
        if (!texture->loadFromFile(path))
        {
            throw std::invalid_argument("No texture available " + path);
        }

        if(_textures.find(id) != _textures.end())
        {
            throw std::invalid_argument("Id " + id + " already exits !");
        }

        _textures[id] = std::move(texture);
        
    }

    sf::Texture* getTexture(const std::string& id)
    {
        try
        {
            return _textures[id].get();
        }
        catch(...)
        {
            throw std::invalid_argument("no texture with the id " + id);
        }
    }
}