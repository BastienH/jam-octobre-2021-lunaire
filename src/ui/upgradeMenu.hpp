#pragma once

#include <SFML/Graphics.hpp>
#include "core/actor.hpp"
#include "progressBar.hpp"
#include <array>
#include "common.hpp"
#include <memory>

struct UpgradeMenu : sf::Drawable, Actor
{
private:
    std::array<ProgressBar, 6> _bars;
    std::array<sf::RectangleShape, 6> _levelUpButtons;
    sf::RectangleShape _selection;
    sf::RectangleShape _bg;
    unsigned _actualSelection;

    static std::unique_ptr<UpgradeMenu> _instance;
public:
    UpgradeMenu();

    void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
    void update(std::chrono::milliseconds dt) override;
    void eventUpdate(sf::Event event);

    static UpgradeMenu & get();
};

 