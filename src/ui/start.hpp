#pragma once

#include <SFML/Graphics.hpp>
#include "core/actor.hpp"
#include <array>
#include "common.hpp"
#include <memory>

struct StartMenu : sf::Drawable, Actor
{
private:
    std::array<sf::RectangleShape, 2> _buttons;
    sf::RectangleShape _selection;
    int _actualSelection;

    //moon
    sf::CircleShape _moonfg, _moonbg;

    //bg
    sf::RectangleShape _bg;

    //text
    sf::Text _start, _quit, _title, _instructions;

    static std::unique_ptr<StartMenu> _instance;
public:
    StartMenu();

    void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
    void update(std::chrono::milliseconds dt) override;
    void eventUpdate(sf::Event event);

    static StartMenu & get();
};