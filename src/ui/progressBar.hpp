#pragma once


#include <SFML/Graphics.hpp>
#include "core/actor.hpp"

#include <array>

struct ProgressBar : sf::Drawable, Actor
{
private:
    static std::array<sf::Color, 5> _colors;
    static constexpr float barWidth = 50;
    static constexpr float barHeight = 75;

    std::array<sf::RectangleShape, 5> _bars =
    {
        sf::RectangleShape(sf::Vector2f(barWidth, barHeight)),
        sf::RectangleShape(sf::Vector2f(barWidth, barHeight)),
        sf::RectangleShape(sf::Vector2f(barWidth, barHeight)),
        sf::RectangleShape(sf::Vector2f(barWidth, barHeight)),
        sf::RectangleShape(sf::Vector2f(barWidth, barHeight))
    };

    sf::RectangleShape bg;
public:
    ProgressBar(int x, int y);
    void draw(sf::RenderTarget&, sf::RenderStates) const override;
    void update(std::chrono::milliseconds) override;
    void setLevel(unsigned level);
};