#include "ui/start.hpp"
#include "core/game.hpp"
#include "moon/moon.hpp"

std::unique_ptr<StartMenu> StartMenu::_instance = nullptr;

constexpr unsigned ButtonWidth = 200;
constexpr unsigned ButtonHeight = 100    ;

StartMenu::StartMenu():
    _buttons{
        sf::RectangleShape(sf::Vector2f(ButtonWidth, ButtonHeight)),
        sf::RectangleShape(sf::Vector2f(ButtonWidth, ButtonHeight))
    },
    _selection(sf::Vector2f(ButtonWidth, ButtonHeight)),
    _actualSelection(0),
    _moonfg(MOON_RADIUS), _moonbg(MOON_RADIUS),
    _bg(WINDOW_SIZE)
{
    for(auto i = 0u; i < _buttons.size(); ++i)
    {
        _buttons[i].setOrigin(ButtonWidth/2, ButtonHeight/2);
        _buttons[i].setPosition((i%2==0 ? -1 : 1) * 220, 0);
        _buttons[i].setOutlineColor(sf::Color::Black);
        _buttons[i].setOutlineThickness(1);
        _buttons[i].setFillColor(sf::Color(90, 30, 30));
    }

    _selection.setOrigin(ButtonWidth/2, ButtonHeight/2);
    _selection.setPosition(_buttons[0].getPosition());
    _selection.setOutlineThickness(5);
    _selection.setOutlineColor(sf::Color::Blue);
    _selection.setFillColor(sf::Color::Transparent);

    //Moon
    _moonbg.setOrigin(MOON_RADIUS, MOON_RADIUS);
    _moonbg.setPosition(0, 0);
    _moonbg.setTexture(core::textureManager::getTexture("moon_bg"));

    _moonfg.setOrigin(MOON_RADIUS, MOON_RADIUS);
    _moonfg.setPosition(0, 0);
    _moonfg.setTexture(core::textureManager::getTexture("moon_fg"));

    //background
    _bg.setOrigin(WINDOW_SIZE.x/2, WINDOW_SIZE.y/2);
    _bg.setPosition(0, 0);
    _bg.setTexture(core::textureManager::getTexture("background"));

    //Text
    _start.setFont(core::game::getFont());
    _start.setString("START");
    _start.setCharacterSize(40);
    _start.setOrigin(_start.getGlobalBounds().width/2, _start.getGlobalBounds().height/2 + 10);
    _start.setPosition(_buttons[0].getPosition());

    _quit.setFont(core::game::getFont());
    _quit.setString("QUIT");
    _quit.setCharacterSize(40);
    _quit.setOrigin(_quit.getGlobalBounds().width/2, _quit.getGlobalBounds().height/2 + 10);
    _quit.setPosition(_buttons[1].getPosition());

    _title.setFont(core::game::getFont());
    _title.setString("Space Billy");
    _title.setCharacterSize(80);
    _title.setOrigin(_title.getGlobalBounds().width/2, _title.getGlobalBounds().height/2 + 10);
    _title.setPosition(0, -180);
    _title.setFillColor(sf::Color::White);

    _instructions.setFont(core::game::getFont());
    _instructions.setString("Press Q or D to move Billy around the moon\nPress SPACE to shoot or to select a menu.");
    _instructions.setCharacterSize(30);
    _instructions.setOrigin(_instructions.getGlobalBounds().width/2, _instructions.getGlobalBounds().height/2 + 10);
    _instructions.setPosition(0, 180);
    _instructions.setFillColor(sf::Color::Black);
}

void StartMenu::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(_bg, states);

    target.draw(_moonfg, states);
    target.draw(_moonbg, states);

    for (auto const & b : _buttons)
        target.draw(b, states);

    target.draw(_start, states);
    target.draw(_quit, states);
    target.draw(_title, states);
    target.draw(_instructions, states);
    
    target.draw(_selection, states);
}

void StartMenu::update(std::chrono::milliseconds)
{
}

void StartMenu::eventUpdate(sf::Event event)
{
    if (event.type == sf::Event::KeyReleased)
    {
        switch (event.key.code)
        {
        case sf::Keyboard::Q:
            --_actualSelection;
            if (_actualSelection < 0)
                _actualSelection = 1;
            break;
        case sf::Keyboard::D:
            ++_actualSelection;
            if (_actualSelection > 1)
                _actualSelection = 0;
            break;
        case sf::Keyboard::Space:
            if(_actualSelection == 0)
            {
                //core::game::_actualScreenType = core::game::ScreenType::InGame;
                core::game::_isStarted = true;
            }
            else
            {
                core::game::stop();
            }
            break;
        default:
            break;
        }

        _selection.setPosition(_buttons[_actualSelection].getPosition());
    }
}

StartMenu & StartMenu::get()
{
    if (!_instance)
        _instance = std::make_unique<StartMenu>();
    return *_instance;
}