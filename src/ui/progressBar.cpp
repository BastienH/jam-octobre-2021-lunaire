#include "progressBar.hpp"

std::array<sf::Color, 5> ProgressBar::_colors{
    sf::Color( 66, 255,  33),
    sf::Color(209, 219,  20),
    sf::Color(240, 191,  14),
    sf::Color(217, 118,   2),
    sf::Color(250,  55,   5)
};

ProgressBar::ProgressBar(int x, int y):
    bg(sf::Vector2f(10 + (barWidth + 10) * 5, barHeight + 20))
{
    bg.setFillColor(sf::Color(125, 125, 125));
    bg.setOutlineColor(sf::Color::Black);
    bg.setOutlineThickness(1);
    bg.setPosition(x, y);
    for(auto i = 0u; i < _bars.size(); ++i)
    {
        _bars[i].setPosition(10 + x + (barWidth + 10) * i, y + 10);
        _bars[i].setOutlineColor(sf::Color::Black);
        _bars[i].setOutlineThickness(1);
        _bars[i].setFillColor(sf::Color(75, 75, 75));
    }
}

void ProgressBar::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(bg, states);
    for (auto const & b : _bars)
        target.draw(b, states);
}

void ProgressBar::update(std::chrono::milliseconds)
{

}

#include <iostream>
void ProgressBar::setLevel(unsigned level)
{
    for (auto i = 0u; i < level; ++i)
        _bars[i].setFillColor(_colors[i]);
}