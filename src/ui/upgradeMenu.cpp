#include "upgradeMenu.hpp"
#include "core/textureManager.hpp"
#include "upgrades/shop.hpp"

std::unique_ptr<UpgradeMenu> UpgradeMenu::_instance = nullptr;

UpgradeMenu::UpgradeMenu():
    _bars{
        ProgressBar(-WINDOW_SIZE.x / 2 + 50, - WINDOW_SIZE.y / 2 + 200), ProgressBar(WINDOW_SIZE.x / 2 - 50 - 310, - WINDOW_SIZE.y / 2 + 200),
        ProgressBar(-WINDOW_SIZE.x / 2 + 50, - WINDOW_SIZE.y / 2 + 350), ProgressBar(WINDOW_SIZE.x / 2 - 50 - 310, - WINDOW_SIZE.y / 2 + 350),
        ProgressBar(-WINDOW_SIZE.x / 2 + 50, - WINDOW_SIZE.y / 2 + 500), ProgressBar(WINDOW_SIZE.x / 2 - 50 - 310, - WINDOW_SIZE.y / 2 + 500)
    },
    _levelUpButtons{
        sf::RectangleShape(sf::Vector2f(95, 95)), sf::RectangleShape(sf::Vector2f(95, 95)),
        sf::RectangleShape(sf::Vector2f(95, 95)), sf::RectangleShape(sf::Vector2f(95, 95)),
        sf::RectangleShape(sf::Vector2f(95, 95)), sf::RectangleShape(sf::Vector2f(95, 95))
    },
    _selection(sf::Vector2f(95, 95)),
    _bg(WINDOW_SIZE),
    _actualSelection(0)
{
    for(auto i = 0u; i < _bars.size(); ++i)
    {
        if (i % 2 == 0) // A gauche
            _levelUpButtons[i].setPosition(-WINDOW_SIZE.x / 2 + 50 + 310 + 20, - WINDOW_SIZE.y / 2 + 200 + (i / 2) * 150);
        else // a droite
            _levelUpButtons[i].setPosition(WINDOW_SIZE.x / 2 - 50 - 310 - 95 - 20, - WINDOW_SIZE.y / 2 + 200 + (i / 2) * 150);
        _levelUpButtons[i].setOutlineColor(sf::Color::Black);
        _levelUpButtons[i].setOutlineThickness(1);
        //_levelUpButtons[i].setFillColor(sf::Color(90, 30, 30));
    }
    _levelUpButtons[0].setTexture(core::textureManager::getTexture("ico_speed"));
    _levelUpButtons[1].setTexture(core::textureManager::getTexture("ico_proj_speed"));
    _levelUpButtons[2].setTexture(core::textureManager::getTexture("ico_rate"));
    _levelUpButtons[3].setTexture(core::textureManager::getTexture("ico_damage"));
    _levelUpButtons[4].setTexture(core::textureManager::getTexture("ico_multibullet"));
    _levelUpButtons[5].setTexture(core::textureManager::getTexture("ico_money"));

    _selection.setPosition(_levelUpButtons[0].getPosition());
    _selection.setOutlineThickness(5);
    _selection.setOutlineColor(sf::Color::Red);
    _selection.setFillColor(sf::Color::Transparent);

    _bg.setPosition(-WINDOW_SIZE.x/2, -WINDOW_SIZE.y/2);
    _bg.setTexture(core::textureManager::getTexture("upgrade_bg"));
}

void UpgradeMenu::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(_bg, states);
    for (auto const & b : _bars)
        target.draw(b, states);
    for (auto const & b : _levelUpButtons)
        target.draw(b, states);
    target.draw(_selection);
}

void UpgradeMenu::update(std::chrono::milliseconds dt)
{
    for (auto & b : _bars)
        b.update(dt);
}

void UpgradeMenu::eventUpdate(sf::Event event)
{
    if (event.type == sf::Event::KeyReleased)
    {
        switch (event.key.code)
        {
        case sf::Keyboard::Z:
            if (_actualSelection > 1)
                _actualSelection -= 2;
            break;
        case sf::Keyboard::Q:
            if (_actualSelection % 2 == 1)
                _actualSelection--;
            break;
        case sf::Keyboard::S:
            if (_actualSelection < 4)
                _actualSelection += 2;
            break;
        case sf::Keyboard::D:
            if (_actualSelection % 2 == 0)
                _actualSelection++;
            break;
        case sf::Keyboard::Space:
            shop::buy(static_cast<shop::UpgradeType>(_actualSelection));
            _bars[_actualSelection].setLevel(shop::_upgrades[_actualSelection].level);
            break;
        default:
            break;
        }

        _selection.setPosition(_levelUpButtons[_actualSelection].getPosition());
    }
}

UpgradeMenu & UpgradeMenu::get()
{
    if (!_instance)
        _instance = std::make_unique<UpgradeMenu>();
    return *_instance;
}